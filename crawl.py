#!/usr/bin/env python3
# This is a JIRA cache pre-loader it goes and looks up a list of JIRA projects.
# Once it has the JIRA projects it goes through each one looking for issues that are not closed/resolved.
# It then crawls all of those individually in an attempt to force the various JIRA caches to warm up.
#
# experimental
#

import requests
import time

JIRA = "https://jira.xxxxx"
AUTH = "username"
PASS = "password"

SLEEP = 14400


def r_wrap(url):
    """
    simple requests wrapper
    """
    if AUTH != "username":
        return requests.get(url, auth=(AUTH, PASS)).json()
    else:
        return requests.get(url).json()


def get_projects():
    """
    returns a list of JIRA project keys
    """
    u = "{JIRA}/rest/api/2/project/".format(JIRA=JIRA)
    return r_wrap(u)


def crawl_project(key):
    """
    Get's a list of all the issues for a given project key
    """
    u = "{JIRA}/rest/api/2/search?jql=project=\"{key}\"&maxResults=200&expand=issues&fields=id,key".format(JIRA=JIRA, key=key)
    return r_wrap(u)


def get_issue(key):
    """
    This goes and grabs the issue via the API
    """
    u = "{JIRA}/rest/api/2/issue/{key}".format(JIRA=JIRA, key=key)
    return r_wrap(u)


def main():
    """
    get the URL and other parameters and then start to process
    """
    print("About to start crawling site: {JIRA}".format(JIRA=JIRA))

    while True:
        # this uses a set to ensure that we only query unique issue keys.
        issues = set()
        for project in get_projects():
            i = project.get('key')
            print("Looking up project: {project}".format(project=i))
            p = dict(crawl_project(i))

            # as we crawl add the issues to a set. This ensures that only unique
            # entries are queried.
            for issue in p.get('issues'):
                issues.add(issue.get('key'))

        # Now using the unique set we query each one and sleep for 3 seconds.
        # This allows for us to not impact other customers on the same hypervisors.
        for issue in issues:
            print("Crawling Issue: {issue}".format(issue=issue))
            get_issue(issue)
            time.sleep(1)

        # clear the set so that we do not cache stale/closed issues.
        issues.clear()
        print("Finished.. Sleeping {SLEEP}".format(SLEEP=SLEEP))
        # sleep for x seconds.
        time.sleep(SLEEP)


if __name__ == '__main__':
    main()
