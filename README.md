## jira-crawl

this is an experimental tool used to evaluate if crawling JIRA improves the performance
of the JIRA instance. It is intended to run in a SCREEN session and outputs logs via stdout.

Further it will wake up every 14400 seconds and re-crawl to ensure that the caches are not expunged. 